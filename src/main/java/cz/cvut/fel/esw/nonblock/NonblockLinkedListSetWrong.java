package cz.cvut.fel.esw.nonblock;

import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Marek Cuchý
 */
@SuppressWarnings("Duplicates")
public class NonblockLinkedListSetWrong implements IntSet {

    private static class Node {

        private final int value;
        private final AtomicReference<Node> next;

        public Node(int value, Node next) {
            this.value = value;
            this.next = new AtomicReference<>(next);
        }
    }

    //empty head and tail nodes
    private final Node head;
    private final Node tail;

    public NonblockLinkedListSetWrong() {
        this.tail = new Node(Integer.MAX_VALUE, null);
        this.head = new Node(Integer.MIN_VALUE, tail);
    }

    public boolean add(int value) {
        Node previous, current;
        while (true) {
            previous = head;
            current = previous.next.get();
            while (value > current.value) {
                previous = current;
                current = current.next.get();
            }
            Node newNode = new Node(value, current);
            if (previous.next.compareAndSet(current, newNode)) {
                return true;
            }
        }
    }

    @Override
    public boolean delete(int value) {
        Node previous, current;
        while (true) {
            previous = head;
            current = previous.next.get();
            while (value > current.value) {
                previous = current;
                current = current.next.get();
            }
            if (current.value == value) {
                if (previous.next.compareAndSet(current, current.next.get())) {
                    return true;
                }
            } else {
                return false;
            }
        }
    }

    @Override
    public void print() {
        StringJoiner stringJoiner = new StringJoiner(" -> ");
        Node curr = head.next.get();
        while (curr != tail) {
//			String pre = curr.next.isMarked() ? "N" : "";
            String pre = "";
            stringJoiner.add(pre + curr.value);
            curr = curr.next.get();
        }
        System.out.println(stringJoiner.toString());
    }

    @Override
    public void validate() {
    }

    @Override
    public int size() {
        int count = 0;
        Node current = head;
        while (current.next.get() != tail) {
            count++;
            current = current.next.get();
        }
        return count;
    }

    /**
     * It finds the largest smaller element than {@code value} and the smallest larger or equal element than {@code
     * value} and stores the results into the input arrays. The arrays workaround for C/C++ pointers.
     *
     * @param prevResult
     * @param currResult
     * @param value
     */
    private void find(Node[] prevResult, Node[] currResult, int value) {
        Node previous = head;
        Node current = previous.next.get();
        while (value > current.value) {
            previous = current;
            current = current.next.get();
        }
        prevResult[0] = previous;
        currResult[0] = current;
    }

    @Override
    public boolean contains(int value) {
        Node curr = head;
        while (value > curr.value) {
            curr = curr.next.get();
        }
        return value == curr.value;
    }
}
