package cz.cvut.fel.esw.nonblock;

/**
 * @author Marek Cuchý
 */
public interface IntSet {

	boolean contains(int value);

	boolean add(int value);

	boolean delete(int value);

	int size();

	void print();

	void validate();
}
