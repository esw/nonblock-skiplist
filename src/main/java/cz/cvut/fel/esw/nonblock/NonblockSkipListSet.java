package cz.cvut.fel.esw.nonblock;

import java.util.StringJoiner;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicMarkableReference;

/**
 * If an integer is contained, it is defined only by the lowest level. The higher levels can be temporarily incomplete.
 *
 * @author Marek Cuchý
 */
public class NonblockSkipListSet implements IntSet {

    private final ThreadLocalRandom rnd;
    private final int maxLevel;

    private final Node head;
    private final Node tail;

    public NonblockSkipListSet(int maxLevel) {
        this.maxLevel = maxLevel;
        this.tail = new Node(this.maxLevel, Integer.MAX_VALUE);
        this.head = new Node(this.maxLevel, Integer.MIN_VALUE, tail);
        this.rnd = ThreadLocalRandom.current();
    }

    @Override
    public boolean contains(int value) {
        //implement a correct check if the value is contained in the skip list. A value is contained in the list if it is in the lowest level of the list and is not marked.
        // start from the top level and continue to right-down
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(int value) {
        Node[] prevResults = new Node[maxLevel + 1];
        Node[] nextResults = new Node[maxLevel + 1];
        Node prev, curr, newNode;
        while (true) {
            //find the previous and next nodes

            //init new node and set its 'next' references at all levels

            //try to insert the new node at the lowest level and if something changed (CAS failed), try again from the start

            //now the value is contained in the set

            //correctly insert the new node at rest of the levels. If a node on a level changes, previous and next nodes have to be found again

            throw new UnsupportedOperationException();
        }

    }

    @Override
    public boolean delete(int value) {
        Node[] prevResults = new Node[maxLevel + 1];
        Node[] nextResults = new Node[maxLevel + 1];
        Node prev, curr;
        while (true) {

            //find the previous and next nodes and node to remove

            //mark all levels (appropriately to how a new node is added). Need to check if next wasn't changed


            throw new UnsupportedOperationException();
        }
    }

    /**
     * It finds the largest smaller element than {@code value} and the smallest larger or equal element than {@code
     * value} for each level and stores the results into the input arrays. Returns true iff {@code value} is contained
     * on the level 0.
     *
     * @param prevResult
     * @param nextResult
     * @param value
     */
    private boolean find(Node[] prevResult, Node[] nextResult, int value) {
        throw new UnsupportedOperationException();
    }

    private int randomLevel() {
        return this.rnd.nextInt(maxLevel + 1);
    }

    @Override
    public void print() {
        for (int level = 0; level <= maxLevel; level++) {
            StringJoiner stringJoiner = new StringJoiner(" -> ", level + ": ", "");
            Node curr = head.next[level].getReference();
            while (curr != tail) {
                String pre = curr.next[level].isMarked() ? "N" : "";
                stringJoiner.add(pre + curr.value);
                curr = curr.next[level].getReference();
            }
            System.out.println(stringJoiner.toString());
        }
    }

    @Override
    public void validate() {
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException();
    }

    private class Node {

        private final int maxLevel;
        private final int value;
        private final AtomicMarkableReference<Node>[] next;

        public Node(int maxLevel, int value) {
            this(maxLevel, value, null);
        }

        @SuppressWarnings("unchecked")
        public Node(int maxLevel, int value, Node defaultValue) {
            this.maxLevel = maxLevel;
            this.value = value;
            this.next = new AtomicMarkableReference[maxLevel + 1];
            for (int i = 0; i < next.length; i++) {
                next[i] = new AtomicMarkableReference<>(defaultValue, false);
            }
        }
    }
}
