package cz.cvut.fel.esw.nonblock;

import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicMarkableReference;

/**
 * @author Marek Cuchý
 */
public class NonblockLinkedListSet implements IntSet {

    private final Node head;
    private final Node tail;

    public NonblockLinkedListSet() {
        Node tail = new Node(Integer.MAX_VALUE, null);
        this.head = new Node(Integer.MIN_VALUE, tail);
        this.tail = tail;
    }

    @Override
    public boolean contains(int value) {
        Node curr = head;
        boolean[] marked = {false};
        while (value > curr.value) {
            curr = curr.next.getReference();
            Node next = curr.next.get(marked);
        }
        return value == curr.value && !marked[0];
    }

    @Override
    public boolean add(int value) {
        Node[] prevResults = new Node[1], currResults = new Node[1];
        Node prev, curr, newNode;
        do {
            find(prevResults, currResults, value);
            prev = prevResults[0];
            curr = currResults[0];
            if (curr.value == value) return false;
            newNode = new Node(value, curr);
        } while (!prev.next.compareAndSet(curr, newNode, false, false));
        return true;
    }

    @Override
    public boolean delete(int value) {
        Node[] prevResults = new Node[1], currResults = new Node[1];
        Node prev, curr;
        while (true) {
            find(prevResults, currResults, value);
            prev = prevResults[0];
            curr = currResults[0];
            if (curr.value == value) {
                Node next = curr.next.getReference();
                boolean marked = curr.next.attemptMark(next, true);
                if (!marked) {
                    continue;
                }
                prev.next.compareAndSet(curr, next, false, false);
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * It finds the largest smaller element than {@code value} and the smallest larger or equal element than {@code
     * value} and stores the results into the input arrays. The arrays workaround for C/C++ pointers.
     *
     * @param prevResult
     * @param currResult
     * @param value
     */
    private void find(Node[] prevResult, Node[] currResult, int value) {
        Node prev, curr, next;
        boolean[] marked = {false};
        retry:
        while (true) {
            prev = head;
            curr = prev.next.getReference();
            while (true) {
                next = curr.next.get(marked);
                // while curr is marked as logically deleted try to delete it physically
                while (marked[0]) {
                    boolean deleted = prev.next.compareAndSet(curr, next, false, false);
                    if (!deleted) {
                        //if any other thread marks prev or changes prev.next start over
                        continue retry;
                    }
                    curr = next;
                    next = curr.next.get(marked);
                }
                if (curr.value >= value) {
                    prevResult[0] = prev;
                    currResult[0] = curr;
                    return;
                }
                prev = curr;
                curr = next;
            }
        }

    }

    @Override
    public void print() {
        StringJoiner stringJoiner = new StringJoiner(" -> ");
        Node curr = head.next.getReference();
        while (curr != tail) {
            String pre = curr.next.isMarked() ? "N" : "";
            stringJoiner.add(pre + curr.value);
            curr = curr.next.getReference();
        }
        System.out.println(stringJoiner.toString());
    }

    @Override
    public void validate() {
        Node curr = head.next.getReference();
        while (curr != tail) {
            if (curr.next.isMarked()) {
                throw new IllegalStateException("One of the elements is marked");
            }
            if (curr.value >= curr.next.getReference().value) {
                throw new IllegalArgumentException("Ordering in the list is violated.");
            }
            curr = curr.next.getReference();
        }
    }

    private static class Node {

        private final int value;

        private final AtomicMarkableReference<Node> next;

        public Node(int value, Node next) {
            this.value = value;
            this.next = new AtomicMarkableReference<>(next, false);
        }


    }

    @Override
    public int size() {
        int count = 0;
        Node current = head;
        while (current.next.getReference() != tail) {
            count++;
            current = current.next.getReference();
        }
        return count;
    }
}
