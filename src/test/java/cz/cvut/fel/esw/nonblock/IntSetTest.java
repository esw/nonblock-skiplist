package cz.cvut.fel.esw.nonblock;

import com.vmlens.api.AllInterleavings;
import org.junit.jupiter.api.Test;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * It has to be run by 'mvn test' or with Eclipse plugin (I'm not aware of any other IDE supported)
 */
public class IntSetTest {

    Supplier<IntSet> testedSet = NonblockLinkedListSetWrong::new;
    //Supplier<IntSet> testedSet = NonblockLinkedListSet::new;
    //Supplier<IntSet> testedSet = NonblockSkipListSet::new;


    /**
     * This test is identical to {@link #testTwoAddsRunner()}
     */
    @Test
    public void testTwoAddsBasic() throws InterruptedException {
        try (AllInterleavings allInterleavings = new AllInterleavings(TestRunner.getMethodName(2))) {
            while (allInterleavings.hasNext()) {
                IntSet set = testedSet.get();
                set.add(15);

                Thread t1 = new Thread(() -> set.add(5));
                Thread t2 = new Thread(() -> set.add(2));

                t1.start();
                t2.start();

                t1.join();
                t2.join();

                assertTrue(set.contains(2));
                assertTrue(set.contains(5));
                assertTrue(set.contains(15));
                assertEquals(3, set.size());
            }
        }
    }

    /**
     * This test is identical to {@link #testTwoAddsBasic()}
     */
    @Test
    public void testTwoAddsRunner() throws InterruptedException {

        TestRunner runner = new TestRunner(testedSet);
        runner.initAdd(15);

        runner.taskAdd(5);
        runner.taskAdd(2);

        runner.assertContains(2);
        runner.assertContains(5);
        runner.assertContains(15);
        runner.assertSize(3);

        runner.executeVmlens();
    }

    @Test
    public void testTwoAddsAlreadyContainedRunner() throws InterruptedException {
        TestRunner runner = new TestRunner(testedSet);

        runner.taskAdd(5);
        runner.taskAdd(5);

        runner.assertContains(5);
        runner.assertSize(1);

        runner.executeVmlens();
    }

}