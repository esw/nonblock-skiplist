package cz.cvut.fel.esw.nonblock;

import com.vmlens.api.AllInterleavings;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Vmlens test runner
 */
public class TestRunner {

    /**
     * Tasks that are executed sequentially before the parallel tasks.
     */
    private final List<Consumer<IntSet>> initTasks = new ArrayList<>();
    /**
     * Tasks to be executed in parallel in the test.
     */
    private final List<Consumer<IntSet>> parallelTasks = new ArrayList<>();

    /**
     * Asserts to be checked sequentially after all the parallel tasks are finished.
     */
    private final List<Consumer<IntSet>> asserts = new ArrayList<>();

    /**
     * Creator of the test to be tested.
     */
    private final Supplier<IntSet> testedSet;

    /**
     * The supplier should always provide new instance
     *
     * @param testedSet supplier of the set to be tested
     */
    public TestRunner(Supplier<IntSet> testedSet) {
        this.testedSet = testedSet;
    }


    public TestRunner taskAdd(int value) {
        return task((set) -> set.add(value));
    }

    public TestRunner taskDelete(int value) {
        return task((set) -> set.delete(value));
    }

    /**
     * Adds a task to be executed in parallel. It runs after the initialization.
     *
     * @param task
     *
     * @return
     */
    public TestRunner task(Consumer<IntSet> task) {
        parallelTasks.add(task);
        return this;
    }

    public TestRunner initAdd(int value) {
        return init((set) -> set.add(value));
    }

    public TestRunner initDelete(int value) {
        return init((set) -> set.delete(value));
    }

    /**
     * Adds a task to be executed as initialized of the set. It runs sequentially before the parallel tasks.
     *
     * @param task
     *
     * @return
     */
    public TestRunner init(Consumer<IntSet> task) {
        initTasks.add(task);
        return this;
    }

    public TestRunner assertContains(int value) {
        return assrt((set) -> assertTrue(set.contains(value), "Assert contains " + value));
    }

    public TestRunner assertNotContained(int value) {
        return assrt((set) -> assertFalse(set.contains(value), "Assert not contains " + value));
    }

    public TestRunner assertSize(int expectedSize) {
        return assrt((set) -> assertEquals(expectedSize, set.size(), "Assert size " + expectedSize));
    }

    public TestRunner assrt(Consumer<IntSet> a) {
        asserts.add(a);
        return this;
    }

    /**
     * If you need correct method name to be calculated for the test, it must be used directly in the test method of
     * which name you want to be displayed in the results. Otherwise use {@link #executeVmlens(String)}
     *
     * @throws InterruptedException
     */
    public void executeVmlens() throws InterruptedException {
        executeVmlens(getMethodName(3));
    }

    public void executeVmlens(String name) throws InterruptedException {
        try (AllInterleavings allInterleavings = new AllInterleavings(name)) {
            while (allInterleavings.hasNext()) {
                execute();
            }
        }
    }

    public void execute() throws InterruptedException {
        IntSet set = testedSet.get();

        initTasks.forEach(init -> init.accept(set));


        List<Thread> threads = parallelTasks.stream()
                                            .map(task -> new Thread(() -> task.accept(set)))
                                            .collect(Collectors.toList());
        threads.forEach(Thread::start);
        for (Thread thread : threads) {
            thread.join();
        }


        asserts.forEach(assrt -> assrt.accept(set));
    }

    public static String getMethodName(int stackLevel) {
        return Thread.currentThread().getStackTrace()[stackLevel].getMethodName();
    }
}
